/*homework5.s*/

	.global _start

_start:
	@ Create Name Text
	MOV R7, #4
	MOV R0, #1
	MOV R2, #17
	LDR R1,=name
	SWI 0

	@ adding 2 and 7
	MOV R2, #2
	MOV R3, #7
	ADD R1, R2, R3
	ADD R1, R1, #48
	LDR R5, =addition
	STR R1, [R5]
	
	@print 9 to screen
	MOV R7, #4
	MOV R0, #1
	MOV R2, #5
	LDR R1, =addition
	SWI 0

	@multiply 3 and 2
	MOV R1, #3
	MOV R2, #2
	MUL R4, R1, R2
	ADD R4, R4, #48
	LDR R3, =multiply
	STR R4, [R3]
	
	@print 14 to screen
	MOV R7, #4
	MOV R0, #1
	MOV R2, #5
	LDR R1, =multiply
	SWI 0

	@logically AND 42 and 7
	MOV R3, #42
	AND R3, R3, #7
	ADD R3, R3, #48
	LDR R4, =logicand
	STR R3, [R4]
	
	@print 2 to screen
	MOV R7, #4
	MOV R0, #1
	MOV R2, #5
	LDR R1, =logicand
	SWI 0

	@logically OR 5 and 2
	MOV R1, #5
	ORR R1, R1, #2
	ADD R1, R1, #48
	LDR R2, =logic2
	STR R1, [R2]
	
	@print 7 to screen
	MOV R7, #4
	MOV R0, #1
	MOV R2, #5
	LDR R1, =logic2
	SWI 0
	
	@Equation (A-B)*C
	MOV R1, #24
	MOV R2, #13
	MOV R3, #3
	SUB R5, R1, R2
	MUL R0, R5, R3
	MOV R7, #1
	SWI 0	



_exit:
	

.data
name:
.ascii "Wyatt Sutherland\n"
addition:
.ascii "    \n"
multiply:
.ascii "    \n"
logicand:
.ascii "    \n"
logic2:
.ascii "    \n"
